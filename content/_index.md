---
title: "CI documentation"
---
Brief introduction
==================

Continuous integration is a software development strategy that aims at
improving the integration work by applying frequent quality controls.
The goal is to improve the global quality of software.

This strategy leverages software testing and good development practices.
Before going further, ensure you have the following conditions:

-   the source code is hosted by a [source code
    manager](http://en.wikipedia.org/wiki/Source_code_management) (Git,
    Mercurial, SVN, \...);
-   your project can be [built
    automatically](http://en.wikipedia.org/wiki/Build_automation)
    (Makefile, Automake, CMake, Ant, Maven, \...);
-   possibly, you have an [automated test
    suite](http://en.wikipedia.org/wiki/Unit_testing) (JUnit, CppUnit,
    \...)

If you think that continuous integration can improve your software but
you do not know how to fulfill the previous conditions, contact your
local experimentation and development service (SED).

If you want to know more about continuous integration, you are seriously
encouraged to read [this
article](http://martinfowler.com/articles/continuousIntegration.html).

User responsibility
===================

Because this platform is shared among all the users, you need to read
the [user charter](page/user_charter "wikilink") and to accept it before
using the service.

You are responsible for the software licenses required on the slaves
(the operating system is also concerned, excepted Microsoft Windows
since Inria provides users with the MSDN Academic Allicance program).

You are also responsible for anything running in the slaves of your
project. Thus, you are supposed to use adequate protections (e.g.
firewall or antivirus) in the slaves to avoid any problem.

How to get started?
===================

If your project fits the requirements presented in the introduction, you
can quickly use the continuous integration platform. Only one hour
should be sufficent to set up your first project.

Some tutorials can help you to perform the several steps to use the
platform:

-   [**Step 1: Web Portal
    Tutorial**](page/web_portal_tutorial "wikilink") → How to create
    your user account, your project and instantiate new slaves (the
    virtual machines that will actually run the jobs of your project).
 -   [**Step 1.bis (optional): CloudStack
    Tutorial**](page/cloudstack_tutorial "wikilink") → How to tune your
    virtual machines. Read this page if you have very specific needs for
    your virtual machines (eg. exotic operating system, more storage
    space, \...).
-   [**Step 2: Slaves Access
    Tutorial**](page/slaves_access_tutorial "wikilink") → How to connect
    to your virtual machines and configure them.
-   [**Step 3: Jenkins Tutorial**](page/jenkins_tutorial "wikilink") →
    How to configure your Jenkins instance

The Jenkins tutorial covers only the most common use cases. If you want
to go deeper with Jenkins, you should [read the official
documentation](https://wiki.jenkins-ci.org/display/JENKINS/Use+Jenkins).

Finally, it is a good idea to subscribe to the following mailing-lists:

-   **[ci-announces](https://sympa.inria.fr/sympa/info/ci-announces)**
    to receive information about the platform status and its updates
-   **[ci-community](https://sympa.inria.fr/sympa/info/ci-community)**
    to share your experience about continuous integration or to request
    help from other users.

Service offer
=============

Inria proposes a service for continuous integrations based both on a
software platform managed by [DGDI](https://intranet.inria.fr/Inria/Directions/Innovation/DGDI/Direction) and on
an hardware platform managed by [DSI](https://intranet.inria.fr/Inria/Directions/Systemes-d-information).

### Software platform

The continuous integration solution is based on Jenkins. [A
portal](https://ci.inria.fr/) has been developed to manage the software
projects. Each project runs its own Jenkins instance, thus the portal is
also used to manage the interactions between a project and a Jenkins
instance (e.g. the user rights management).

### Hardware platform

To perform the continuous integration tasks, each Jenkins instance can
access to slaves. Instead of using directly real computers, the platform
leverages the virtualization capabilities and uses a cloud middleware to
offer flexibility in terms of computing availability and operating
system choice. For more information about the platform, see [this
page](https://wiki.inria.fr/ci/Archi_en).

![](/doc/img/Hw-platform.png "/doc/img/Hw-platform.png")

Provided templates (non exhaustive):
- [Windows 10](page/windows10-template), Windows 7
- Mac OS X 10.9
- Ubuntu 18.04, CentOS 6.3, Fedora 29

### Availability

Because the platform relies on an hardware part and on shared networks
equipments, failures may occur. As far as we are not able to offer high
availability mechanisms that ensure 24/7 running without downtime, we
offer a best effort working service. Furthermore, DSI promises to deal
with hardware or network problems as far as possible and a team is able
to operate all the working days.

In case of problem, do not hesitate to write a mail to
ci-support@inria.fr. Your request will be processed in average in a
half working day.
