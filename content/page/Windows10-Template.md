# Introduction

The `windows10-choco-java-virtio` template (in the `Featured` tab) provides a Windows
10 installation with a native OpenSSH server and Chocolatey.

You may connect a fresh instance of this template via [SSH](#SSH) or
[rdesktop](#rdesktop).

SSH connections use the `cmd.exe` as default shell. You may [change
the default shell](#change-the-default-shell) for Powershell, or the [Cygwin
`bash`](#install-cygwin), or the `bash` provided by the [Windows subsystem for
Linux](#install-windows-subsystem-for-linux).

[Chocolatey] is a package manager for Windows. You may use it to
install the development tools that you need.

[Chocolatey]: https://chocolatey.org/

- [Java](#install-java-runtime)
- [Git](#install-git)
- [Microsoft Visual Studio](#install-microsoft-visual-studio)
- [Cygwin](#install-cygwin)
- [CMake](#install-cmake)

and many other packages.

# Connect a slave via SSH

Add these lines to your `~/.ssh/config` file (by substituting
`‹login›` with your actual login on `ci.inria.fr`).

```
Host *.ci
ProxyCommand ssh -W $(basename %h .ci):%p ‹login›@ci-ssh.inria.fr
```

The following command connect the instance.


```
ssh ci@‹project›-‹slave›.ci 
```

# Connect a slave via rdesktop

The connection via rdesktop relies on an SSH tunnel to the rdp port.

```
ssh ‹login›@ci-ssh.inria.fr -L 3380:‹project›-‹slave›:3389
```

You may choose any other free port number instead of `3380` if you
want to open SSH tunnels to several slaves simultaneously.

If you obtain an error relative to bind
```
bind [::1]:3380: Cannot assign requested address
```
add a `-4` to ssh to be sure to use IPV4 when connecting


`rdesktop` can now be opened on that port on `localhost`. `rdesktop`
is packaged by many distributions. For example, you may install it
with apt on Debian or Ubuntu, or homebrew[1] on Mac OS X.

[1] https://brew.sh/

With apt (Debian, Ubuntu, ...):
```
apt install rdesktop
```

With homebrew (Mac OS X):
```
brew install rdesktop
```

For cut/copy/paste to work with `rdesktop` on Mac OS X (within the
remote slave itself or between the remote slave and the client
machine), you may have to select the `CLIPBOARD` selection as
target for the remote clipboard, by launching `rdesktop` with
the following arguments.

```
rdesktop -r clipboard:CLIPBOARD localhost:3380
```

# Change the default shell

The initial default shell when opening an SSH connection is
`cmd.exe`. This default can be changed by setting the
string value `DefaultShell` in the registry key
`HKEY_LOCAL_MACHINE\SOFTWARE\OpenSSH`.
For example, the following Powershell command line sets
Powershell as the default shell.
For more details about configuring OpenSSH, see
https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_server_configuration

Warning: Jenkins expects `cmd.exe` command line on Windows
environment.  Making Powershell the default shell will prevent
Jenkins from using the agent.

```
New-ItemProperty -Path "HKLM:\SOFTWARE\OpenSSH" -Name DefaultShell -Value "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -PropertyType String -Force
```

# Install Java runtime

The following command installs Java runtime, required for
Jenkins.
More details about `javaruntime` package:
https://chocolatey.org/packages/javaruntime

```
choco install -y javaruntime
```

# Install Git

The following command installs Git.
More details about `Git` package:
https://chocolatey.org/packages/Git

```
choco install -y git
```

# Install Microsoft Visual Studio

The following command installs a minimal setup for Visual Studio
2017 Community.
More details about `VisualStudio2017Community` package:
https://chocolatey.org/packages/VisualStudio2017Community

```
choco install -y visualstudio2017community --execution-timeout=0
```

The following command installs Visual C/C++ compiler.  Visual
Studio 2017 Community (or another base package) should have already
been installed before running this command.  More details about
`visualstudio2017-workload-nativedesktop` package:
https://chocolatey.org/packages/visualstudio2017-workload-nativedesktop

```
choco install -y visualstudio2017-workload-nativedesktop
```

The following command adds x64 native tools (e.g., the `cl.exe`
compiler) to the `PATH` variable of the current shell environment.

```
"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars64.bat"
```

# Install Cygwin

The following command installs a minimal setup for Cygwin.
More details about `Cygwin` package:
https://chocolatey.org/packages/Cygwin

```
choco install -y cygwin
```

Once Cygwin is installed, Cygwin packages can be installed
via `choco` command line with option `--source cygwin`.
For instance, to install GNU Make:

```
choco install --source cygwin make
```

The following command runs the Cygwin environment, which by
default provides a `bash` command line.

```
C:\tools\cygwin\Cygwin.bat
```

The following Powershell command makes Cygwin the
default shell for SSH connections.

```
New-ItemProperty -Path "HKLM:\SOFTWARE\OpenSSH" -Name DefaultShell -Value "C:\tools\cygwin\Cygwin.bat" -PropertyType String -Force
```

If you already connect Jenkins to the agent, remember
to disconnect then reconnect from Jenkins to use Cygwin.
The remote working directory shoud be set to `/cygdrive/c/builds`
and you should create this directory before trying to connect
to the agent via Jenkins.

# Install CMake

The following command installs CMake and add `CMake` to `PATH`
for all users.
More details about `cmake` package:
https://chocolatey.org/packages/cmake

```
choco install -y cmake --installargs 'ADD_CMAKE_TO_PATH=System'
```

You should run the Powershell command `Reload-Service sshd`
to get the new environment for subsequent SSH connections.
Moreover, if you already connect Jenkins to the agent, remember
to disconnect then reconnect from Jenkins as well.

# Install Windows subsystem for Linux

The Windows subsystem for Linux is an optional feature, you
may enable it by installing the package `wsl`.
https://chocolatey.org/packages/wsl

```
choco install -y wsl
```

You will probably have to restart Windows before being able
to use the subsystem. If subsequent installations of
some distributions fail, you may try to restart before trying
again.

Alternatively, you may enable the feature in Powershell as
administrator.

```
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
```

You may then install the distribution of your choice. Some
examples:

```
choco install -y wsl-ubuntu-1604
```

```
choco install -y wsl-debiangnulinux
```

These packages have `wsl` package as dependency: the
feature Windows subsystem for Linux will therefore be enabled
if required when you request to install these packages.
However, `choco` appears not to restart Windows before
trying to install the distributions, leading their
installation to fail if you don't restart Windows
yourself before.

The command `ubuntu` launches Ubuntu default shell
(presumably, bash).
The Debian package does not appear to insert `debian`
command in `PATH`; the command can be found in
`C:\ProgramData\chocolatey\lib\wsl-debiangnulinux\tools\unzipped\debian.exe`.

The following Powershell command makes Ubuntu default
shell the default shell for SSH
connections.

```
New-ItemProperty -Path "HKLM:\SOFTWARE\OpenSSH" -Name DefaultShell -Value "C:\ProgramData\chocolatey\bin\ubuntu.exe" -PropertyType String -Force
```

If you already connect Jenkins to the agent, remember
to disconnect then reconnect from Jenkins to use WSL.
The remote working directory shoud be set to `/mnt/c/builds`
and you should create this directory before trying to connect
to the agent via Jenkins.

You will have to install the Java runtime in the
subsystem.

```
apt install default-jre
```

# Configure Jenkins

To configure Jenkins launch method “Launch agent agents via SSH”:
* the “Host” field should match the hostname,
* the “Credentials” field should be the “ci/ci” login/password pair.
* the “Host Key Verification Strategy” should be “Manually provided key Verification Strategy”, and the associated “SSH Key” should be the content of one of the keys generated in `C:\ProgramData\ssh\*.pub`. You may copy the key locally before with `scp`.

```
scp ci@‹project›-‹slave›.ci:'C:\ProgramData\ssh\ssh_host_rsa_key.pub' .
```

Note: it does not appear to be possible to use key authentication
and the credential “ci (Inria CI generic authentication to slaves)”.
Theoretically, the key is downloadable at the following URL:
https://ci.inria.fr/project/download/ssh/‹project›
and the public SSH key of the Jenkins instance should be added to the file
`C:\ProgramData\ssh\administrators_authorized_keys` on the slave
(or `C:\Users\ci\.ssh\authorized_keys` if the last lines of `sshd_config`
have been commented).
Moreover, the `authorized_keys` file should have the correct permissions:
Microsoft provides a tool for that:
```
Set-ExecutionPolicy RemoteSigned
Install-Module -Force OpenSSHUtils -Scope AllUsers
Repair-AuthorizedKeyPermission C:\Users\ci\.ssh\authorized_keys
```
However, sshd keeps showing `Authentication error` in the logs.

# Disable automatic update via Windows Update service

Windows Update automatically detects, downloads and installs update
for Windows, which can be useful, in particular for security updates.
However, updating Windows often implies automatic restarts that can be
unconvenient for continuous integration.  Windows Update service can
be disabled in Powershell with the following command:
```
Stop-Service wuauserv
Set-Service -Name wuauserv -StartupType 'Manual'
```

# Resize disk

First, you have to resize the volume in Cloudstack: on the volume
panel, click on the last button of the toolbar (with the tooltip
"Resize Volume"). In the form, you can ignore the first line ("New
Offering") and supply the second line with a size in GB ("New Size
(GB)").

Then, you have to open a graphical connection to the slave to resize
the partition. Run `diskmgmt.msc`, select the partition to resize (the
partition should be immediately on the left of the "Unallocated"
space), and choose "Extend Volume..." in the context menu. On the
second page of the assistant, copy in "Select the amount of space in
MB:" the value of "Maximum available space in MB".

On some Windows installation, there may be a recovery partition
between the main partition and the unallocated space, which prevents
the main partition to be resized by the above method. You can delete
the recovery partition by using the command-line tool `diskpart`
instead of `diskmgmt.msc`:

- run `rescan` first

- then `list disk`, then `select disk` _n_

- then `list partition`, then `select partition` _n_

- then `delete partition override`
