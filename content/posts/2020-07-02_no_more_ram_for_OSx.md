+++
title =  "Saving resources for MacOS X slaves"
date = "2020-07-02T16:03:49+01:00"
tags = ["featured"]
featured_image = ""
description = ""
+++

Please read this message if you use or intent to use a slave running
MacOSX on CloudStack.

We are getting short in RAM on the VMWare infrastructure used to provide
MacOS slaves on CloudStack. We are doing our best to get more RAM, but
it would also help if the unused MacOS slaves would be deleted by their
owners (until a bit more RAM becomes available, it will be impossible to
create new MacOSX slaves on CloudStack).

So, if you have created a MacOSX slave on CloudStack and it turns
out you do not really need it, we would be grateful if you could take
just a few minutes to remove it through the CI portal:

https://ci.inria.fr

Go to the page of your project, then choose "Manage slaves", stop the
slave you would like to delete and finally delete it.

Many thanks for your understanding.

We will send an update as soon as it becomes available to us.
