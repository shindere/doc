---
title: "Slaves Access Tutorial"
---
Any 'slave' virtual machine created on <https://ci.inria.fr/> (or
<https://ci-cloud.inria.fr>) is accessible by ssh bouncing through the
SSH gateway **ci-ssh.inria.fr**. This page explains how to connect
through SSH to a virtual machine.

Once connected, you will be able to perform any administration tas (like
installing the packages you need to build your software).

**Note:** some packages (JDK, Ant and Maven especially) can be
[automatically installed by
Jenkins](../jenkins_tutorial#setting-up-tools-.28e.g.-jdk.2c-maven.29 "wikilink").
You may prefer not to install them manually.

# Prerequisites

## Register you ssh public key

The SSH gateway **ci-ssh.inria.fr** only allows authentication by public
key.

If you do not already have a SSH key pair, you first need to create it.
Once it is created you can register it in your user account.

### Create a SSH Key

The procedure to create a SSH key pair depends on your operating system:

-   [create a key with the `ssh-keygen`
    command](http://www.grid.ie/using/sshkey-howto.html) (Linux/Mac-OS
    users)
-   [create a key with Putty's Key
    Generator](http://siteadmin.gforge.inria.fr/ssh_windows.html)
    (Windows users)

### Upload your SSH Key to the portal

To upload your SSH key the CI portal click on your email on the upper
right then **My account**:

1.  log in with your account on the [web portal](https://ci.inria.fr/login)

2.  click on the button displaying your e-mail (on the top-right of the
    page) and select **My Account**

    ![](/doc/img/CI-CloudStack-CIMyAccount.png "fig:/doc/img/CI-CloudStack-CIMyAccount.png")

3.  paste your public key into the the text area **Add an SSH Key** and
    click on **Add this key**

    ![](/doc/img/CI-CloudStack-AddSSHKey.png "fig:/doc/img/CI-CloudStack-AddSSHKey.png")

## Identify your user name

The user name you must use to open a session on the SSH gateway can be
found on the **My account** page of the CI portal, it displayed in the
**Uid** field.

![](/doc/img/CI-CloudStack-CIUid.png "fig:/doc/img/CI-CloudStack-CIUid.png")

## Identify your virtual machine

A virtual machine can be reached either using its IP address or using
its hostname:

You can obtain the the IP address or hostname of your slave:

-   in the Web Portal: they are displayed in the [**Slaves** tab of your
    project](/doc/page/web_portal_tutorial/#slaves)

    ![](/doc/img/CI-Portal-IP-Hostname.png "fig:/doc/img/CI-Portal-IP-Hostname.png")

-   in the [Cloudstack interface](/doc/page/cloudstack_tutorial/): in the instances tab

    ![](/doc/img/CI-CloudStack-IP-Hostname.png "fig:/doc/img/CI-CloudStack-IP-Hostname.png")

VM host names follow the pattern: *project\_name*-*slave\_name*. If the
project name is **my\_project** and the slave name is **s1**, the
resulting hostname will be **my\_project-s1**.

# Linux/MacOS users

## Connecting to a Linux/MacOSX slave

### Basics

To connect a Linux (or any Unix) slave:

1.  ssh to **ci-ssh.inria.fr** with [your user
    name](#identify-your-user-name "wikilink")
2.  once you are logged in ci-ssh.inria, ssh to [your virtual
    machine](#identify-your-virtual-machine "wikilink") using the
    **root** or **ci** account

Example:

`$ ssh `[`mdam001`](#identify-your-user-name "wikilink")`@ci-ssh.inria.fr`\
`Last login: Thu Jul 19 18:24:20 2012 from 194.199.1.234`\
`-sh-4.1$ ssh root@`[`my_project-s1`](#identify-your-virtual-machine "wikilink")\
`root@my_project-s1's password: `

**Note:** The default user accounts in linux templates are:

-   user: **root** password: **password** *(except for Ubuntu 16.04 , where the
    root account is disabled for security reason)*
-   user: **ci** password: **ci**

**Note:** The default user accounts in Mac OS X templates are:

-   user: **ci** password: **ci**
-   root access can be granted using sudo (password: **ci**)

### Configure a ssh proxy command

Connecting manually to the SSH gateway before accessing to your slave is
quite inconvenient. You can automate this by adding a proxy command into
your `~/.ssh/config` file:

`Host *.ci`\
`    ProxyCommand ssh `[*`username`*](#identify-your-user-name "wikilink")`` @ci-ssh.inria.fr "/usr/bin/nc `basename %h .ci` %p" ``

Now the previous command becomes:

`$ ssh root@`[`my_project-s1`](#identify-your-virtual-machine "wikilink")`.ci`\
`root@my_project-s1.ci's password:`

### Connecting to a MacOSX slave (graphical way)
It is possible to connect to MacOSX slaves using VNC. You could use this
solution if the Cloudstack console does not work.

1. On the **slave**

  Start the vnc service on your slave with [`kickstart`](https://support.apple.com/en-us/HT201710), connecting by `ssh`:
```bash
ssh ci@*slave_name*.ci
sudo  /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart \
      -activate -configure -access -on -clientopts \
      -setvnclegacy  -vnclegacy yes -clientopts -setvncpw -vncpw mypasswd \
      -restart -agent -privs -all
```

2. On the **client**
    *  create an SSH tunnel to the slave's port 5900 with the following command:

    `ssh -L 9999:`[`*slave_name*`](#identify-your-virtual-machine "wikilink")`:5900 `[`*username*`](#identify-your-user-name "wikilink")`@ci-ssh.inria.fr`
    *  launch a vnc client
       - from OS X: on Finder, hit Command+K, enter `vnc://localhost:9999` and click 'Connect'
       - from another OS:
           * use [https://www.realvnc.com/download/viewer/](vncviewer) `vncviewer localhost:9999`
           * other alternatives are gvncviewer, TigerVNC, ...

## Connecting to a Windows slave

### Windows slaves: RDP access through a SSH tunnel

If you want to connect to a windows slave using Remote Desktop, you have
to create a SSH tunnel with the following command:

`ssh -L 3389:`[*`slave_name`*](#identify-your-virtual-machine "wikilink")`:3389 `[*`username`*](#identify-your-user-name "wikilink")`@ci-ssh.inria.fr`

For example for a windows whose name is my\_project-s1:

`$ ssh -L 3389:my_project-s1:3389 mdam001@ci-ssh.inria.fr`

You can then connect to **localhost** using the Remote Desktop client in
a new command window (rdesktop uses the default 3389 port that will be
redirected by the SSH tunnel).

`rdesktop localhost`

On Windows 10, you can face the following error message :
`ERROR: CredSSP: Initialize failed, do you have correct kerberos tgt initialized ?`
You will have to fix the
[CredSSP](https://www.syskit.com/blog/credssp-required-by-server-solutions/).
We advise to allow insecure connections without NLA (network-level
authentication).

**Note:** the default password for the account 'ci' of the windows
templates is 'ci'

**Note for Ubuntu users**: Remmina remote desktop client is installed by
default. We recommend to use rdesktop instead.

**Note for MacOS X users**: the rdesktop client is not well supported on
MacOS X. We recommend to use [the CoRD remote desktop
client](http://cord.sourceforge.net/) instead.

**Some useful rdesktop options:**
- if rdesktop client opens too small or too big
windows, you can fix it with the option:
```bash
# Open a window with a size of 90% of your screen setup - useless on dual screen
rdesktop -g 90% localhost
# or
# Open a window with a specific size
rdesktop -g 1920x1536 localhost
```
- if rdesktop client keyboard layout is not correct:
```bash
# For a french keyboard - can be combined with -g option
rdesktop -k fr localhost
```

### Windows slaves: web-based RDP access through CloudStack

If you have trouble setting up a SSH tunnel for native RDP traffic, you
may connect to a Windows slave using CloudStack's web-based client.

To do so, log in on CloudStack (e.g. through the link on your CI project
page), go to the 'instances' tab, select your Windows slave and click
on the 'shell' icon. This will start a RDP session.

### Windows slaves: SSH on Windows slaves

You will have to install sshd on the Windows slave.
You have two solutions, depending if you want a Unix environment or not.

#### Installing a Unix environment and a sshd daemon

You can install MSYS2
(https://sourceforge.net/p/msys2/wiki/MSYS2%20introduction/) and follow
the instructions to install SSH server `sshd` running with `cygrunsrvd`
are here : <https://gist.github.com/samhocevar/00eec26d9e9988d080ac>.

Do not forget to create `/etc/passwd` file before running the
installation script.

This solution is the robustest, but the environment of ssh connection is
Unix.

#### Installing a Windows daemon based on OpenSSH

You can install OpenSSH for Windows, which is in beta development by
Microsoft.

The instructions to install Win32 OpenSSH are here :
<https://github.com/PowerShell/Win32-OpenSSH/wiki/Install-Win32-OpenSSH>

# Windows users

For Windows users, if you are not familiar with SSH
configuration/connection, please refer to
[Putty](http://www.chiark.greenend.org.uk/~sgtatham/putty/) and [its
documentation](http://the.earth.li/~sgtatham/putty/0.62/htmldoc/).

## Connecting to a Linux slave

![](/doc/img/CloudStack-PuttyProxy.png "/doc/img/CloudStack-PuttyProxy.png")

To connect directly to the previous virtual machine, you need to use the
local proxy feature of Putty. To do so, go to the **Connection \>
Proxy** tab of the connection configuration, select **Local** as proxy
type, put **ci-ssh.inria.fr** as proxy hostname and use the following
**local proxy command**:

` plink.exe %proxyhost -l `[*`username`*](#identify-your-user-name "wikilink")` -agent -nc %host:%port\n`

![](/doc/img/CloudStack-PuttyConnect.png "/doc/img/CloudStack-PuttyConnect.png")

Then go to the **Session** panel, type in the host name and **Open**. Of
course, for all that to work, you need to have correctly [set up your
key-pair
authentication](http://www.ualberta.ca/CNS/RESEARCH/LinuxClusters/pka-putty.html).
Then you will have to authenticate on the virtual machine as user
'root' or 'ci'.

**Note:** The default user accounts in linux templates are:

-   user: **root** password: **password** *(except for Ubuntu, where the
    root account is disabled for security reason)*
-   user: **ci** password: **ci**

## Connecting to a Windows slave

If you want to connect to a windows slave using Remote Desktop, you have
to create a SSH tunnel with the following command on a console:

`plink.exe -L 3380:`[*`slave_name`*](#identify-your-virtual-machine "wikilink")`:3389 `[*`username`*](#identify-your-user-name "wikilink")`@ci-ssh.inria.fr`

The local port 3380 is used in order to avoid problem with the local
service. For example for a windows whose name is my\_project-s1:

`plink.exe -L 3380:my_project-s1:3389 mdam001@ci-ssh.inria.fr`

You can then connect to **localhost:3380** using the Remote Desktop
client.

On Windows 10, you can face the following error message :
`FATAL ERROR: Remote side unexpectedly closed network connection`
You will have to fix the
[CredSSP](https://www.syskit.com/blog/credssp-required-by-server-solutions/).
We advise to allow insecure connections without NLA (network-level
authentication).

**Note:** the default password for the account 'ci' of the windows
templates is 'ci'

# Next step

You have now completed the second step of the tutorial! You can proceed
to final step of the tutorial.

[→ Jenkins tutorial](../jenkins_tutorial "wikilink")

This part will teach you the basics about Jenkins: creating new jobs and
configuring them to run on the virtual machines you have just created.
