+++
title =  "CI Service interruption for Mac OS VMs on Wednesday 17 - Thursday 18 March"
date = "2021-03-05T11:08:29+01:00"
tags = ["featured"]
+++
In order to increase the Mac OS virtualization capacities of the CI
cloud (more CPU cores and more RAM available for Mac OS virtual
machines), new Mac OS hosts will be deployed on Wednesday 17 -
Thursday 18 March.

During these two days, it will be impossible to create new Mac OS
virtual machines, and existing Mac OS virtual machines may become
unavailable or restarted without notice. We are sorry for these
inconveniences.

Thank you for your understanding!
