+++
title =  "Windows 10 template"
date = "2021-02-23T12:47:12+01:00"
tags = ["featured"]
+++

The Windows 10 template `windows10-choco-java-virtio2` is available
for more than a year now, and provides minimal Windows settings with
the native OpenSSH server preconfigured and the package manager
[Chocolatey](https://chocolatey.org) for installing software.

The template comes with a documentation, which gives commands to
install Visual Studio, Cygwin, and other common software.
https://ci.inria.fr/doc/page/windows10-template/

You may even have a look on how the template has been configured over
a vanilla Windows 10 installation.
https://ci.inria.fr/doc/page/windows10-template-create/
