+++
title =  "CI portal unavailable on Tuesday 16 March: cloudstack upgrade v4.11→v4.13"
date = "2021-03-03T16:56:22+01:00"
tags = ["featured"]
+++

The cloudstack software that manages CI virtual machines will be
upgraded on Tuesday 16 March (from v4.11 to v4.13): the CI portal will
likely be unavailable on that day, and in particular creating new
virtual machines or projects will be impossible. However, existing
virtual machines and Jenkins instances will still be available as
usual. If you need new virtual machines, please create them before or
wait until the end of the upgrade!

The upgrade will make ISO image uploading easier and will allow
virtual machines to be created with “custom” offers, by choosing
manually and independently the number of cores and the amount of RAM.

As a side note, thanks to many efforts performed by our users, a
notable amount of memory has been freed on the VMware hypervisor: if
you were blocked for creating new Mac OS virtual machines, you can now
try again.

Thank you for using CI!
